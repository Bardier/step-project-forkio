Список использованных технологий:
HTML, SASS, JavaScript, NodeJS, Gulp, BEM

Состав участников проекта: Bardier Nick, Varlam Maria

Задания выполнял Bardier Nick:
Сверстать шапку сайта с верхним меню (включая выпадающее меню при малом разрешении экрана). Сверстать секцию People Are Talking About Fork. Настройка Gulp для проекта.

Задания выполняла Varlam Maria:
Сверстать блок Revolutionary Editor. Сверстать секцию Here is what you get. Сверстать секцию Fork Subscription Pricing.